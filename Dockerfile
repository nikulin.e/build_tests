FROM busybox:1.36.1-glibc

RUN set -x && \
    mkdir /data && \
    echo 'test' > /data/test.txt && \
    echo 'mega' > /data/mega.txt && \
    ls -l /data

VOLUME /out

CMD cp /data/* /out

